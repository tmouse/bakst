$(function() {

	var Zoomapp = (function(){

		return {
			init : function() {

				Zoom.init();
				Flickering.init();
				if (isMobile.any){
					$('body').addClass('mobile');
				}
			}
		}
	})()


	,Zoom = (function(){
		var $big = $('.panzoom.big-img__wrap'),//.find('img'),
			$small = $('.panzoom.small-img__wrap').find('img'),
				$zoomIn = $('.zoom').find(".zoom-in"),
				$zoomOut = $('.zoom').find(".zoom-out"),
				$zoomRange = $('.zoom').find(".zoom-range input"),
				$zoomLens = $('#lens'),
			config = {
				scale_big: {
					min : 1,
					max : 3,
				},
				scale_small:{
					min : 0.21,
					max : 1,
				},
				koef : 0.22,
				lensKoef: $small.width() / $big.width(),
				canvas_w: $('.panzoom.big-img__wrap').width(),
				canvas_h: $('.panzoom.big-img__wrap').height()
			}

		return {
			init : function() {
				$(window).on('resize',function(){
					config.canvas_w = $('.panzoom.big-img__wrap').width();
					config.canvas_h = $('.panzoom.big-img__wrap').height();
				});

				var api = {};

				api.panzoom = function($obj, matrix ) {
					$obj.panzoom("setMatrix", matrix, {
						animate: true,
						range: true,
						silent : false
					});
				};
				api.getMatrix = function() {
					return $big.panzoom("getMatrix");
				};
				api.getScale = function( matrix ) {
					matrix = matrix || api.getMatrix();
					return parseFloat(matrix[0]);
				};
				api.getShift = function( matrix, scale ) {
					matrix = matrix || api.getMatrix();
					scale = scale || api.getScale( matrix );
					return {
						x : 100 * parseFloat(matrix[4]) / (config.canvas_w * scale),
						y : 100 * parseFloat(matrix[5]) / (config.canvas_h * scale)
					};
				};
				api.lensRecalc = function(matrix){

					var scale = api.getScale( matrix );
					var shift = api.getShift( matrix, scale );
					var new_dim = 100/scale; // new_dim = new_width = new_height
					//console.log(matrix, scale, shift,  new_dim);
					var center = { x : 50, y : 50 };
					center.x = center.x - shift.x;
					center.y = center.y - shift.y;

					var left = center.x - new_dim/2;
					var top  = center.y - new_dim/2;

					$zoomLens.css({
						width  : new_dim + '%',
						height : new_dim + '%',
						top    : top + '%',
						left   : left + '%'
					});

				};
				api.reset = function() {
					$big.panzoom("reset");
				};
				api.goTo = function(n){
					var $this = $('[data-info-slide='+n+']'),
						matrix = $this.attr('data-zoom-matrix').split(',');
					api.panzoom($big, matrix);
					var scale =  api.getScale();
					$zoomRange.val(scale);
					$zoomRange.rangeslider('update', true);
					$('[data-info-slide]').removeClass('active');
					$('[data-slide]').removeClass('active');
					$('[data-slide='+n+']').addClass('active');
				};
				api.itemHighlight = function(n){
					$('[data-info-slide]').removeClass('active');
					$('[data-info-slide='+n+']').addClass('active');
				};

				$big.attr('data-real-width', $big.width()).panzoom({
					$zoomIn: $zoomIn,
					$zoomOut: $zoomOut,
					$zoomRange: $zoomRange,
					startTransform: 'scale('+config.scale_big.min+')',
					minScale: config.scale_big.min,
					maxScale: config.scale_big.max,
					//increment: 0.2,
					contain        : 'invert',
					onChange: function(){
						var M = $big.panzoom('getMatrix').slice(0, 6);
						api.lensRecalc(M);
						var scale = api.getScale();
						if (scale !== 1){
							$big.addClass('hover-disabled');
						} else {
							$big.removeClass('hover-disabled');
						}
					}
				}).panzoom('zoom', config.scale_big.min);

				$zoomLens.draggable({containment:$('#drag-lens')});

				var $dragging_lens = null;
				var lens_drag_point = { top : 0, left : 0};
				$zoomLens.on('mousedown', function(e){
					var $temp_dragging = $(e.target);
					if ( $temp_dragging.is ( $zoomLens ) ) {
						$dragging_lens = $temp_dragging;
						//$appdoc.addClass('non-selectable');

						var lens_offset = $zoomLens.offset();
						lens_drag_point.top = e.pageY - lens_offset.top;
						lens_drag_point.left = e.pageX - lens_offset.left;
					}
				}); // on mousedown

				$(document).on('mouseup', function(){
					if ( $dragging_lens ) {
						$dragging_lens = null;
						//$appdoc.removeClass('non-selectable');
					}
				}); // on mouseup
				var $minimap = $('#drag-lens'),
						X0 = $minimap.offset().left,
						Y0 = $minimap.offset().top,
						W0 = $minimap.width(),
						H0 = $minimap.height();

				$zoomLens.on( "drag", function( event, ui ) {
					var $draggingLens = $(this);
						var lw = $draggingLens.width(),
								lh = $draggingLens.height(),
								x0 = event.pageX,
								y0 = event.pageY;
						var edge_left   = x0 - lens_drag_point.left - X0,
								edge_top    = y0 - lens_drag_point.top - Y0,

								center_d_x = ( W0/2 - (edge_left + lw/2) ) / W0,
								center_d_y = ( H0/2 - (edge_top + lh/2) ) / H0;

						var scale = api.getScale();

						var dx = config.canvas_w * scale * center_d_x;
						var dy = config.canvas_h * scale * center_d_y


						$big.panzoom("pan", dx, dy);


				}); // on drag
				$(document).on('click','.zoom-in,.zoom-out',function(){
					var scale =  api.getScale();
					$zoomRange.val(scale);
					$zoomRange.rangeslider('update', true);
				});
				$('[data-home-link]').on('click',function(e){
					e.preventDefault();
					$big.panzoom('reset');
					$('[data-slide]').removeClass('active');
					$('[data-slide="-1"]').addClass('active');
				});
				$(document).on('click','div:not(.hover-disabled) [data-info-slide]',function(e){
					e.preventDefault();
					var n = $(this).attr('data-info-slide');
					console.log(n);
					api.goTo(n);
				});
				$(document).on('touchstart touchmove','div:not(.hover-disabled) [data-info-slide]',function(e){
					e.preventDefault();
					$(this).addClass('active');
				});
				$(document).on('touchstart touchmove','div:not(.hover-disabled) [data-info-slide]', function(e){
					e.preventDefault();
					Flickering.hoverDemoPause();
					var $this = $(this);

					window.setTimeout(function(){
						var n = $this.attr('data-info-slide');
						api.goTo(n);

					}, 400);
				});
			}
		}
	})()
			,Flickering = (function(){
				var api = {};
				var hoverdemo = {
					timer    : null,
					pos      : null,
					duration : 2000,
					status   : false
				};
				var $wrap = $('#big-img');

				var $slides = $('[data-info-slide]');
				api.itemHighlight = function(n){
					$('[data-info-slide]').removeClass('active');
					$('[data-info-slide='+n+']').addClass('active');
				};
				api.hoverDemoStart = function( pos_start ) {

					// reset timer (diso bugfix)
					window.clearInterval( hoverdemo.timer );

					var pos_end   = 5;//$slides.length;

					hoverdemo.pos = pos_start ? pos_start : 0;

					hoverdemo.timer = window.setInterval(function(){
						api.itemHighlight(hoverdemo.pos);
						hoverdemo.pos += 1;

						if ( hoverdemo.pos == pos_end ) {
							hoverdemo.pos = 0;
						}
					}, hoverdemo.duration);

					hoverdemo.status = true;
				}

				api.hoverDemoIsStarted = function() {
					return hoverdemo.status;
				}

				api.hoverDemoPause = function() {
					$('[data-info-slide].active').removeClass('active');
					if ( hoverdemo.timer ) {
						window.clearTimeout( hoverdemo.timer );
					}
				};
				api.hoverDemoContinue = function() {
					api.hoverDemoStart( hoverdemo.pos );
				}
				api.hoverDemoStop = function() {
					$('[data-info-slide].active').removeClass('active');
					if ( hoverdemo.status ) {
						window.clearTimeout( hoverdemo.timer );
						hoverdemo.pos = 0;
						hoverdemo.status = false;
					}
				}

				//events
				api.handleEvents = function() {

					// Overlay hover behaviour
					$wrap
							.on('mouseenter mouseover click', function(){
								if ( api.hoverDemoIsStarted() ) {
									api.hoverDemoPause();
								}
							})
							.on('mouseleave', function(){
								//if ( api.hoverDemoIsStarted() ) {
										api.hoverDemoStart(0);
								//}
							})
				};
				$(document).on('mousemove',function (e){
					var id = e.target.id;
					if (id == 'big-pic'){
						window.setTimeout(function(){
							if ( api.hoverDemoIsStarted() ) {
								api.hoverDemoPause();
							}
						}, 2000);
					}

				});

				api.init = function() {

					api.handleEvents();
					api.hoverDemoStart();
				};

				return api;
			})()
	/**
	 * Dummy Module Example
	 */
		,DummyModule = (function(){
			return {
				init : function() {
					// do something
				}
			}
		})()

		;Zoomapp.init();

});

