$(function() {

	var App = (function(){

		return {
			init : function() {

				Detect.init();
				Range.init();
				Templating.init();
			}
		}
	})()

			,Detect = (function(){
				var oldies = {
					'chrome': 15,
					'msie' : 10,
					'firefox': 15,
					'safari': 5
				};
				return {
					init : function() {

						var name = BrowserDetect.browser.name,
								ver = BrowserDetect.browser.value,
								isOld = false;
						$.each(oldies, function(key, val){
							if (key == name){
								if (ver.split('.')[0] < val){
									isOld = true;
								}
							}
						});
						isOld ? $('#app-wrap').addClass('oldie') : '' ;
					}
				}
			})()
			,Range = (function(){
				var $range = $('[data-zoom-range]');
				return {
					init : function() {
						$range
								.rangeslider({
									polyfill: false,
								});
					}
				}
			})()
			,Templating = (function(){
				var data = {
					items: [
						{
							name : 'flash',
							'zoom-matrix': '1.2, 0, 0, 1.2, 44.8875, 52',
							img: 'images/hovers/01-flash.png',
							frames: ['<div class="canvas-i__frame"></div>']
						},
						{
							name : 'varrior',
							'zoom-matrix': '3, 0, 0, 3, 532, -242.37',
							img: 'images/hovers/02-varrior.png',
							frames: ['<div class="canvas-i__frame"></div>']
						},
						{
							name : 'ruin',
							'zoom-matrix': '1.3, 0, 0, 1.3, 19.9575, -58.7913',
							img: 'images/hovers/03-ruin.png',
							frames: ['<div class="canvas-i__frame"></div>',
										'<div class="canvas-i__frame"></div>',
										'<div class="canvas-i__frame"></div>']
						},
						{
							name : 'hand',
							'zoom-matrix': '3, 0, 0, 3, 98.6267, -520',
							img: 'images/hovers/04-hand.png',
							frames: ['<div class="canvas-i__frame"></div>']
						},
						{
							name : 'head',
							'zoom-matrix': '3, 0, 0, 3, 115.252, -431.621',
							img: 'images/hovers/05-head.png',
							frames: ['<div class="canvas-i__frame"></div>']
						},
					],
					index: function() {
						return ++window['INDEX']||(window['INDEX']=0);
					}
				};
				return {
					init : function() {

						var output = $("#canvas");
						var template = $("#template").html();
						var html = Mustache.render(template, data);
						output.append(html);

					}
				}
			})()
	/**
	 * Dummy Module Example
	 */
		,DummyModule = (function(){
			return {
				init : function() {
					// do something
				}
			}
		})()

		;App.init();

});

